import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
interface Result {
  identifier: string;
  description?: string;
  date_published?: Date;
  headline?: string;
  name?: string;
}

export async function updatePeriodical(
  database: Database,
  identifier: string,
  updatedProperties: {
    description?: string;
    headline?: string;
    name?: string;
  },
): Promise<Result> {
  const propertiesToUpdate = [];
  if (typeof updatedProperties.description === 'string') {
    propertiesToUpdate.push('description');
  }
  if (typeof updatedProperties.headline === 'string') {
    propertiesToUpdate.push('headline');
  }
  if (typeof updatedProperties.name === 'string') {
    propertiesToUpdate.push('name');
  }

  // tslint:disable-next-line:no-invalid-template-strings
  const updateClauses = propertiesToUpdate.map((key) => key + '=${' + key + '}').join(',');

  const result: Result = await database.one(
    `UPDATE periodicals SET ${updateClauses}`
      // tslint:disable-next-line:no-invalid-template-strings
      + ' WHERE identifier=${identifier}'
      + ' RETURNING identifier, name, headline, description, date_published',
    {
      ...updatedProperties,
      identifier,
    },
  );

  return result;
}
